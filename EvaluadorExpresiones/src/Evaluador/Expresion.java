/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "  <->  ";
        }
        return msg;
    }

    public String getPrefijo() {
        Pila<String> p = new Pila();
        ListaCD<String> l = expresiones;
        String exp;
        for (int i = 0; i < expresiones.getTamanio(); i++) {

        }
        return "";
    }

    public String getPosfijo() {
        String posfijo = "";
        boolean fin = false;
        Pila<String> signos = new Pila();
        for (int i = 0; i < expresiones.getTamanio(); i++) {
            String aux = expresiones.get(i);
            switch (aux) {
                case "(":
                    signos.apilar(aux);
                    break;
                case "^":
                    signos.apilar(aux);
                    break;
                case "/":
                    if (signos.esVacia()) {
                        signos.apilar(aux);
                    } else if ("+".equals(signos.getTope()) || signos.getTope() == "-" || signos.getTope() == "(") {
                        signos.apilar(aux);
                    } else {
                        posfijo += signos.desapilar() + ",";
                        signos.apilar(aux);
                    }
                    break;
                case "*":
                    if (signos.esVacia()) {
                        signos.apilar(aux);
                    } else if ("+".equals(signos.getTope()) || signos.getTope() == "-" || signos.getTope() == "(") {
                        signos.apilar(aux);
                    } else {
                        posfijo += signos.desapilar() + ",";
                        signos.apilar(aux);
                    }
                    break;
                case "+":
                    if (signos.esVacia()) {
                        signos.apilar(aux);
                    } else if ("(".equals(signos.getTope())) {
                        signos.apilar(aux);
                    } else {
                        posfijo += signos.desapilar() + ",";
                        signos.apilar(aux);
                    }
                    break;
                case "-":
                    if (signos.esVacia()) {
                        signos.apilar(aux);
                    } else if ("(".equals(signos.getTope())) {
                        signos.apilar(aux);
                    } else {
                        posfijo += signos.desapilar() + ",";
                        signos.apilar(aux);
                    }
                    break;
                case ")":
                    while (!"(".equals(signos.getTope())) {
                        posfijo += signos.desapilar() + ",";
                    }
                    signos.desapilar();
                    break;
                default:
                    posfijo += aux + ",";
                    break;
            }
        }
        if (!signos.esVacia()) {
            while (!signos.esVacia()) {
                posfijo += signos.desapilar() + ",";
            }
        }
        posfijo = posfijo.substring(0, posfijo.length() - 1);
        return posfijo;
    }

    public float getEvaluarPosfijo() {
        Pila<String> numeros = new Pila();
        Float num1, num2;
        String[] expresion = getPosfijo().split(",");
        for (String dato : expresion) {
            if (!"^".equals(dato) && !"/".equals(dato) && !"*".equals(dato) && !"+".equals(dato) && !"-".equals(dato) && !"%".equals(dato)) {
                numeros.apilar(dato);
            }
            if ("+".equals(dato)) {
                num2 = Float.parseFloat(numeros.desapilar());
                num1 = Float.parseFloat(numeros.desapilar());
                num1 += num2;
                String aux = num1.toString();
                numeros.apilar(aux);
            }
            if ("-".equals(dato)) {
                num2 = Float.parseFloat(numeros.desapilar());
                num1 = Float.parseFloat(numeros.desapilar());
                num1 -= num2;
                String aux = num1.toString();
                numeros.apilar(aux);
            }
            if ("/".equals(dato)) {
                num2 = Float.parseFloat(numeros.desapilar());
                num1 = Float.parseFloat(numeros.desapilar());
                num1 /= num2;
                String aux = num1.toString();
                numeros.apilar(aux);
            }
            if ("*".equals(dato)) {
                num2 = Float.parseFloat(numeros.desapilar());
                num1 = Float.parseFloat(numeros.desapilar());
                num1 *= num2;
                String aux = num1.toString();
                numeros.apilar(aux);
            }
            if ("^".equals(dato)) {
                num2 = Float.parseFloat(numeros.desapilar());
                num1 = Float.parseFloat(numeros.desapilar());
                Double d = Math.pow(num1, num2);
                numeros.apilar(d.toString());
            }
        }
        return Float.parseFloat(numeros.desapilar());
    }
}
